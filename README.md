# `fphw` LaTeX class for homeworks

This is a class dedicated to format your homework. It is designed to be easy to read, and easy to grade.

It is heavily based on the standard `article` class and it accepts all the same optional arguments, except for `twoside` and `twocolumn`, which difficult reading of the finished document. An `spanish`option is added to support the use of spanish; **this does not remove the need for the babel package**.

The default paper size is A4; this can be changed to Letter using `letterpaper` in the options of the class.

Packages `geometry`, `fancyhdr` and `titlesec` are required. All other packages need to be loaded at the preamble of your document.

## Features available 

Commands added that **can** to be used in the preamble are:
- `\institute{}`  indicates the institution the homework this is going to be turned in to,
- `\professor{}` to indicate the name of the professor,
- `\class{}` to indicate the course this homework is intended to be delivered.

You **have** to provide a title, author and due date trough the `\title{}`, `\author{}` and `\date{}` commands respectively. There is no point in using this class if you don't, therefore the author recommends that if this is the case, you search for something else :(

The `problem` environment is available to put a problem (or anything you want) inside a box.

